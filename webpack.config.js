const fs = require('fs');
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//const Inputmask = require('inputmask');
const ROOT_DIR = fs.realpathSync(process.cwd());

function pathResolve(...args) {
    return path.resolve(ROOT_DIR, ...args);
}
const isDev = process.env.Node_ENV !== 'production';

function getName(mode){
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.js`
}
module.exports ={
    mode: isDev ? 'development' : 'production',
    entry: {
        main:'./src/index.js'
    },
    output:{
        filename: getName('js'),
        path: pathResolve('dist')
    },
    resolve:{
        extensions: ['.js','.json','.jsx']
    },
    module:{
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test:/\.css$/,
                use:[
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: 'file-loader',
                options: {
                    name: 'assets/[contenthash].[ext]',
                },

            }
        ]
    },

    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: pathResolve('src/index.html'),
            chunks: ['main']
        }),
        new MiniCssExtractPlugin()
    ],
    devServer: {
        port:'3000',
        open:true
    }
};
