import './scss/style.scss';
import Inputmask from "inputmask";
import gif from './images/loader.gif';
import icon from './images/icon.png';
import visa from './images/visa.png';
import mastercard from './images/mastercard.png';

console.log(gif);
console.log(gif.src);
Inputmask({"mask": "9999 9999 9999 9999", "placeholder": ""}).mask(".box-card-number"); //masks for inputs
Inputmask({"mask": "99/99 ", "placeholder": ""}).mask(".box-card-date");
Inputmask({"mask": "999", "placeholder": "" }).mask(".box-card-cvv");

$( "#cardNumber" ).change(function() { //detecting visa ot mastercard
    let inputNumber = document.getElementById('cardNumber');
    let number = inputNumber.value;
    number=number.replace(/\s/g, '');
    if(numberValidation(number)){
        console.log(1);
        if(number.charAt(0)=="5"){
            console.log(5);
            inputNumber.style.background =`white url(${mastercard}) no-repeat 95% center`;
            inputNumber.style.backgroundSize='30px';
        }
        if(number.charAt(0)=="4"){

            console.log(4);
            inputNumber.style.background =`white url(${visa}) no-repeat 95% center`;
            inputNumber.style.backgroundSize='30px';
        }
    }else{
        inputNumber.style.background='white';
    }
});

function numberValidation(value){ // The Luhn Algorithm number verification
    if (/[0-9]{16}/.test(value)===false)
        return false;
    let nCheck = 0, nDigit = 0, bEven = false;
    value = value.replace(/\D/g, "");

    for (let n = value.length - 1; n >= 0; n--) {
        let cDigit = value.charAt(n),
            nDigit = parseInt(cDigit, 10);

        if (bEven) {
            if ((nDigit *= 2) > 9) nDigit -= 9;
        }

        nCheck += nDigit;
        bEven = !bEven;
    }

    return (nCheck % 10) == 0;
}

function dateValidation(date){ // validating date of card
    console.log( date[0]);
    if ((/[0-9]{2}/.test(date[0]) && /[0-9]{2}/.test(date[0]))===false)
        return false;
    date[0]=parseInt( date[0], 10);
   let todayYear = new Date().getFullYear().toString().substr(-2);
   let todayMonth = new Date().getMonth()+1;
    if(date[0]<0 || date[0]>12)
        return false;
    if(date[1]<todayYear){
        return false;
    } else if(date[1]===todayYear && date[0]>todayMonth){
        return true;
    }
}

function cvvValidation(cvv){ // validating cvv of card
    if (/[0-9]{3}/.test(cvv)===false)
        return false;
}

async function loader(){
    let button = document.getElementById('submit');
    button.value="";
    button.style.background = `rgb(122,182,240) url(${gif}) no-repeat  center`;
    button.style.backgroundSize='25px';
    const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));
    await timeout(2000);
    button.value="Получить деньги";
     button.style.background = 'linear-gradient(122.5deg, #5A4BE6 -33.07%, #73AFF7 48.35%, #93D0D9 139.94%)';
        alert("Поздравляем, вам одобрен кредит");
}

$( "#submit" ).click(function() { // on click on submit button - validating data
    let validInputs = 0;
    let inputNumber = document.getElementById('cardNumber');
    let inputDate = document.getElementById('cardDate');
    let inputCvv = document.getElementById('cardCvv');
    let cardLabel = document.getElementById('cardNumberLabel');
    let dateLabel = document.getElementById('cardDateLabel');
    let cvvLabel = document.getElementById('cardCvvLabel');
    let number = inputNumber.value;
    let date = inputDate.value;
    let cvv = inputCvv.value;
    number=number.replace(/\s/g, '');
    date = date.split('/');
    if(numberValidation(number)===false){
        cardLabel.style.display = 'block'; //if verification fails
        inputNumber.style.border='1px solid #FF8D8D'
    } else {
        cardLabel.style.display = 'none'; //if verification success
        inputNumber.style.border='1px solid #CCCCCC';
        validInputs++;
    }

    if(dateValidation(date)===false){
        dateLabel.style.display = 'block';
        inputDate.style.border='1px solid #FF8D8D'
    } else {
        dateLabel.style.display = 'none';
        inputDate.style.border='1px solid #CCCCCC';
        validInputs++;
    }

    if(cvvValidation(cvv)===false){
        cvvLabel.style.display = 'block';
        inputCvv.style.border='1px solid #FF8D8D'
    } else {
        cvvLabel.style.display = 'none';
        inputCvv.style.border='1px solid #CCCCCC';
        validInputs++;
    }

    if(validInputs===3) //if all fields are verified
        loader();

});

